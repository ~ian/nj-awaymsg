
package AwayMsg;

use strict;
use warnings;

use DBI;
use Data::Dumper;

BEGIN {
    use Exporter ();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
    $VERSION     = 1.00;
    @ISA         = qw(Exporter);
    @EXPORT      = qw(db_connect db_commit www_begin hquote
                      $dbh);
    %EXPORT_TAGS = ( );
    @EXPORT_OK   = qw();
}

our ($dbh);

sub www_begin ($$) {
    my ($r,$m) = @_;
    $r->header_out("Cache-Control: no-cache");
}

sub hquote ($) {
    my ($raw) = @_;
    return pack "H*", $raw;
}

sub db_connect () {
    my $dbf = "$ENV{'NJAWAYMSG'}/data/away.db";
    $dbh = DBI->connect("dbi:SQLite:$dbf",'','',
			 { AutoCommit=>0,
			   RaiseError=>1, ShowErrorStatement=>1
			 })
        or die "$DBI::errstr ?";
}

sub nooutput ($) {
    my ($stmt) = @_;
    my $sth= $dbh->prepare($stmt);
    $sth->execute();
    my $row;
    if ($row= $sth->fetchrow_hashref()) {
	die("REFERENTIAL INTEGRITY ERROR\n".
	    "\n$stmt\n". Dumper($row),"\n");
    }
}

sub db_commit () {
    nooutput("SELECT * FROM addresses LEFT JOIN config".
	     " USING (emailaddr) WHERE forwardfile IS NULL");
    nooutput("SELECT * FROM addresses LEFT JOIN texts".
	     " USING (textid) WHERE desc IS NULL");
    $dbh->do("COMMIT");
}

1;
