#!/usr/bin/perl -w

use HTML::Mason::CGIHandler;
use strict;
use POSIX;

my $script_filename = $ENV{'SCRIPT_FILENAME'};
my $basis = shift @ARGV;
sub absify ($) { $_[0] =~ m#^/# ? $_[0] : "$basis/$_[0]"; }
my $perllib = absify shift @ARGV;
my $comp_root = absify shift @ARGV;

unshift @INC, $perllib;

my $pathsofar = $comp_root;
die "$pathsofar $!" unless stat $pathsofar;

$ENV{'PATH_INFO'} ||= '/main';
my $path_info = $ENV{'PATH_INFO'};

foreach my $pathcomp (split m#/#, $path_info) {
    next if $pathcomp eq '';
    $pathcomp =~ m/^[,_0-9A-Za-z]/ or path_fail('bad initial character');
    $pathcomp =~ m/[-+:.,_0-9A-Za-z]$/ or path_fail('bad final character');
    $pathcomp ne 'autohandler' or path_fail('refers to autohandler');
    $pathcomp ne 'dhandler' or path_fail('refers to dhandler');
    $pathsofar .= "/$pathcomp";
    next if access($pathsofar, X_OK);
    die "$pathsofar $!" unless $!==&ENOENT or $!==&ENOTDIR;
    last;
}

our $h;
$h or $h = HTML::Mason::CGIHandler->new(comp_root => $comp_root);
$h->handle_request();
